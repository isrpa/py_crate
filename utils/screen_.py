# coding: utf8

"""
屏幕类
"""

import win32api

def get_size():
    """屏幕分辨率大小
    
    返回类似1920,1080"""
    return win32api.GetSystemMetrics(0), win32api.GetSystemMetrics(1)
