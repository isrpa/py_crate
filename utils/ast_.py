# coding: utf8
"""
扩展ast模块便捷方法
"""

import ast


def literal_eval(string: str):
    """字符串转语句表达式
    表达式从其他遍历拼接而来时，注意使用repr()函数，例如：
    s = "I'm bear!"
    literal_eval(f"list({repr(s)})")    # 'list("I\'m bear!")'
    # ['I', "'", 'm', ' ', 'b', 'e', 'a', 'r', '!']
    """
    parsed = ast.parse(string, mode="eval")
    fixed = ast.fix_missing_locations(parsed)
    compiled = compile(fixed, "<string>", "eval")
    return eval(compiled)
