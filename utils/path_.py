# coding: utf8
"""
路径处理工具
"""

import os
from pathlib import Path, PurePosixPath, PureWindowsPath


class AbsolutePath(Path):
    """绝对路径对象"""

    def __new__(cls, *args):
        if cls is AbsolutePath:
            cls = WindowsAbsolutePath if os.name == 'nt' else PosixAbsolutePath
        self = super(AbsolutePath, cls).__new__(cls, *args)
        # 针对只传递盘符C:不带/情况无法获取绝对路径情况
        if not self.absolute().is_absolute():
            self = cls(os.path.join(self, os.sep))
        return self.absolute()

    def split(self: Path):
        """
        拆分路径的目录与文件
        :return: 返回(目录, 文件)元组
        """
        return self.parent.as_posix(), self.name

    def splitext(self: Path, keep_dot=False):
        """
        分割文件名与扩展名
        :param keep_dot: 是否保留扩展名前的.点
        :return: 返回(文件名, 扩展名)元组
        """
        if not keep_dot:
            return self.stem, self.suffix.replace(".", "", 1)
        return self.stem, self.suffix

    def with_ext(self: Path, ext=""):
        """
        以扩展名更改路径
        :param ext: 扩展名，如csv：/a/b.txt -> /a/b.csv
        :return: 修改后的路径
        """
        if ext:
            return self.with_suffix(f".{ext}")
        return self

    def with_stem(self: Path, stem=""):
        """
        以文件名词干(不含文件格式的名称)修改文件名
        :param stem: 词干名，如：123.txt -> abc.txt
        :return: 修改后的路径
        """
        return self.with_name(f"{stem}{self.suffix}")

    @property
    def ext(self):
        """文件扩展名"""
        return self.suffix.replace(".", "", 1)

    @property
    def str(self):
        """
        转成字符串，替代as_posix()方法
        :return: 返回路径字符串值
        """
        return self.as_posix()


class PosixAbsolutePath(AbsolutePath, PurePosixPath):
    __slots__ = ()


class WindowsAbsolutePath(AbsolutePath, PureWindowsPath):
    __slots__ = ()

    def owner(self):
        raise NotImplementedError("Path.owner() is unsupported on this system")

    def group(self):
        raise NotImplementedError("Path.group() is unsupported on this system")
