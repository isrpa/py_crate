# coding: utf8
"""
获取RPA流程中前一个组件的属性信息，需在PIP管理中额外安装astor库
"""

import ast
import inspect
import json

import pandas as pd

try:
    import astor
except:
    raise Exception("需在PIP管理中额外安装astor库，"
                    "菜单栏-工具-PIP管理-左下角+号-输入astor-安装按钮")


def getPrevCompProp():
    """
    将此函数放入RPA设计器的【全局函数】中，然后在画布中调用此【全局函数】，
    注意：必须要放在一个RPA组件后使用

    :return:
    """
    # # 该全局函数被调用在哪里，获取调用者所在文件和行
    cur_frame = inspect.currentframe()
    call_frame = inspect.getouterframes(cur_frame, 2)
    callme_file, lineno_in_file = call_frame[1][1], call_frame[1][2]
    print(f"{callme_file}, {lineno_in_file}")

    # # 读取调用者文件内容到df中，便于后续查找上一个组件
    with open(callme_file, "r", encoding="utf8") as fp:
        df = pd.DataFrame(fp.readlines(), columns=["line"])

    # # 从当前行开始向上找，使用ast解析是否ubpa组件，是则返回其属性参数信息
    comp_prop = {}
    prev_lineno = lineno_in_file - 1
    for lineno in range(prev_lineno, 0, -1):
        index = lineno - 1
        line_code = df.loc[index]["line"].strip()
        try:
            ast_node = ast.parse(line_code)
            if isinstance(ast_node.body[0].value, ast.Call):
                func_pkg_name = ast_node.body[0].value.func.value.id
                if df[df["line"].str.contains(func_pkg_name)
                      & df["line"].str.contains("import")
                      & df["line"].str.contains("ubpa")].empty:
                    continue
                func_name = ast_node.body[0].value.func.attr
                comp_prop["comp"] = f"{func_pkg_name}.{func_name}"
                comp_prop["prop"] = {kw.arg: astor.to_source(kw.value).strip()
                                     for kw in ast_node.body[0].value.keywords}
                print(json.dumps(comp_prop, indent=4, ensure_ascii=False))
                return comp_prop
        except Exception as e:
            continue
    return comp_prop
